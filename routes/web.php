<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\WelcomeController;

Route::get('/', function(){
    return 'Home';
});

Route::get('/usuario', [UserController::class, 'index']);

Route::get('/usuario/{id}/', [UserController::class, 'show'])
    ->where('id', '[0-9]+');

Route::get('/usuario/nuevo', [UserController::class, 'create']);

Route::get('/saludo/{name}/{nickname?}', [WelcomeController::class, 'index']);
