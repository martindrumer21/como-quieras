<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class WelcomeUsersTest extends TestCase
{
    /** @test */
    public function it_welcomes_users_with_nickname()
    {
        $this->get('/saludo/martin/mart')
            ->assertStatus(200)
            ->assertSee('Bienvenido Martin, tu apodo es mart');
    }

    /** @test */
    public function it_welcomes_users_without_nickname()
    {
        $this->get('/saludo/martin')
            ->assertStatus(200)
            ->assertSee('Bienvenido Martin');
    }
}
